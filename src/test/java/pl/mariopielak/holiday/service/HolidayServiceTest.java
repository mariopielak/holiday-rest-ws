package pl.mariopielak.holiday.service;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.mariopielak.holiday.exception.HolidayException;
import pl.mariopielak.holiday.json.CountryHoliday;
import pl.mariopielak.holiday.json.HolidayResponse;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static junitparams.JUnitParamsRunner.$;
import static org.mockito.Mockito.when;
import static pl.mariopielak.holiday.test.HolidayResponseAssert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class HolidayServiceTest {

    @Mock
    private CountryHolidayWSClient holidayWSClient;

    private HolidayService objectUnderTest;

    @Before
    public void initialize() {
        MockitoAnnotations.initMocks(this);
        objectUnderTest = new HolidayService(holidayWSClient);
    }

    private Object dataForOneYearTest() {
        List<CountryHoliday> first = Arrays.asList(
                new CountryHoliday("Party", LocalDate.of(2012, 1, 1)),
                new CountryHoliday("SomePartyDayUS", LocalDate.of(2012, 6, 1)));
        List<CountryHoliday> second = Arrays.asList(
                new CountryHoliday("Party", LocalDate.of(2012, 3, 1)),
                new CountryHoliday("SomePartyDayFI", LocalDate.of(2012, 6, 1)));
        return $(
                $(first, second)
        );
    }

    @Test
    @Parameters(method = "dataForOneYearTest")
    @TestCaseName("{0}, {1}")
    public void shouldFindNextCommonHolidayInFirstYear(List<CountryHoliday> first, List<CountryHoliday> second) {
        // given
        when(holidayWSClient.getHolidays("US", 2012)).thenReturn(first);
        when(holidayWSClient.getHolidays("FI", 2012)).thenReturn(second);
        // when
        HolidayResponse commonHolidayResponse = objectUnderTest.findCommonHoliday("FI", "US", LocalDate.of(2012, 1, 1));
        // then
        assertThat(commonHolidayResponse)
                .hasName1("SomePartyDayFI")
                .hasName2("SomePartyDayUS")
                .hasDate(LocalDate.of(2012, 6, 1));
    }

    private Object dataForTwoYearTest() {
        List<CountryHoliday> first1stYear = Arrays.asList(
                new CountryHoliday("Party1", LocalDate.of(2012, 1, 1)),
                new CountryHoliday("Party2", LocalDate.of(2012, 6, 1)));
        List<CountryHoliday> second1stYear = Arrays.asList(
                new CountryHoliday("PartyOK1", LocalDate.of(2012, 3, 1)),
                new CountryHoliday("PartyOK2", LocalDate.of(2012, 5, 1)));
        List<CountryHoliday> first2ndYear = Arrays.asList(
                new CountryHoliday("Party3", LocalDate.of(2013, 1, 1)),
                new CountryHoliday("Party4", LocalDate.of(2013, 3, 1)));
        List<CountryHoliday> second2ndYear = Arrays.asList(
                new CountryHoliday("PartyOK3", LocalDate.of(2013, 2, 1)),
                new CountryHoliday("PartyOK4", LocalDate.of(2013, 3, 1)));
        return $(
                $(first1stYear, second1stYear, first2ndYear, second2ndYear)
        );
    }

    @Test
    @Parameters(method = "dataForTwoYearTest")
    @TestCaseName("{0}, {1}, {2}, {3}")
    public void shouldFindNextCommonHolidayInSecondYear(List<CountryHoliday> first1stYear, List<CountryHoliday> second1stYear,
                                                        List<CountryHoliday> first2ndYear, List<CountryHoliday> second2ndYear) {
        // given
        when(holidayWSClient.getHolidays("PL", 2012)).thenReturn(first1stYear);
        when(holidayWSClient.getHolidays("GR", 2012)).thenReturn(second1stYear);
        when(holidayWSClient.getHolidays("PL", 2013)).thenReturn(first2ndYear);
        when(holidayWSClient.getHolidays("GR", 2013)).thenReturn(second2ndYear);

        // when
        HolidayResponse commonHolidayResponse = objectUnderTest.findCommonHoliday("PL", "GR", LocalDate.of(2012, 1, 1));
        // then
        assertThat(commonHolidayResponse)
                .hasName1("Party4")
                .hasName2("PartyOK4")
                .hasDate(LocalDate.of(2013, 3, 1));
    }


    private Object dataWithoutCommonHoliday() {
        List<CountryHoliday> first = Arrays.asList(
                new CountryHoliday("Party", LocalDate.of(2009, 1, 1)),
                new CountryHoliday("SomePartyDayUS", LocalDate.of(2009, 4, 1)));
        List<CountryHoliday> second = Arrays.asList(
                new CountryHoliday("Party", LocalDate.of(2009, 3, 1)),
                new CountryHoliday("SomePartyDayFI", LocalDate.of(2009, 5, 1)));
        return $(
                $(first, second)
        );
    }

    @Test (expected = HolidayException.class)
    @Parameters(method = "dataWithoutCommonHoliday")
    @TestCaseName("{0}, {1}")
    public void shouldThrowAnExceptionWhenCommonHolidayNotFound(List<CountryHoliday> first, List<CountryHoliday> second) {
        // given
        when(holidayWSClient.getHolidays("US", 2009)).thenReturn(first);
        when(holidayWSClient.getHolidays("FI", 2009)).thenReturn(second);
        // when
        objectUnderTest.findCommonHoliday("FI", "US", LocalDate.of(2009, 1, 1));
        // then
        // should throw an exception, verify by annotation property
    }
}
