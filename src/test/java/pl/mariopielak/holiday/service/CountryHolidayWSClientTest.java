package pl.mariopielak.holiday.service;

import okhttp3.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import pl.mariopielak.holiday.exception.HolidayException;
import pl.mariopielak.holiday.json.CountryHoliday;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(initializers = ConfigFileApplicationContextInitializer.class)
public class CountryHolidayWSClientTest {

    private static final String TEST_CORRECT_JSON =
            "{\"status\":200,\"holidays\":{" +
                    "\"2009-01-01\":[{\"name\":\"Nytårsdag\",\"date\":\"2009-01-01\",\"observed\":\"2009-01-01\",\"public\":true}]," +
                    "\"2009-04-05\":[{\"name\":\"Palmesøndag\",\"date\":\"2009-04-05\",\"observed\":\"2009-04-05\",\"public\":false}]}}";

    private static final String TEST_ERROR_JSON =
            "{\"status\":402,\"error\":\"Free accounts are limited to historical data..... \"}";

    private static final LocalDate TEST_2009_01_01 = LocalDate.of(2009, 1, 1);

    @Autowired
    @InjectMocks
    private CountryHolidayWSClient objectUnderTest;

    @Mock
    private OkHttpClient okHttpClientMock;

    @Mock
    private Call callMock;

    @Before
    public void initialize() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldGetNextHoliday() throws IOException {
        //given
        when(okHttpClientMock.newCall(any())).thenReturn(callMock);
        when(callMock.execute()).thenReturn(createResponse(200, TEST_CORRECT_JSON));

        //when
        List<CountryHoliday> response = objectUnderTest.getHolidays("DK", 2009);

        // then
        assertThat(response)
                .hasSize(2)
                .contains(new CountryHoliday("Nytårsdag", LocalDate.of(2009, 1, 1)))
                .contains(new CountryHoliday("Palmesøndag", LocalDate.of(2009, 4, 5)));
    }

    @Test(expected = HolidayException.class)
    public void shouldThrowAnExceptionWhenResponseIsWrong() throws IOException {
        //given
        when(okHttpClientMock.newCall(any())).thenReturn(callMock);
        when(callMock.execute()).thenReturn(createResponse(402, TEST_ERROR_JSON));

        //when
        objectUnderTest.getHolidays("US", 2009);

        // then
        // should throw an exception, verification is done by annotation property
    }

    @Test(expected = HolidayException.class)
    public void shouldThrowAnExceptionWhenResponseIsNull() throws IOException {
        //given
        when(okHttpClientMock.newCall(any())).thenReturn(callMock);
        when(callMock.execute()).thenReturn(null);

        //when
        objectUnderTest.getHolidays("US", 2009);

        // then
        // should throw an exception, verification is done by annotation property
    }

    private Response createResponse(int httpCode, String jsonBody) {
        return new Response.Builder()
                .body(ResponseBody.create(MediaType.parse("application/json"), jsonBody))
                .request(new Request.Builder().url("http://url").build())
                .protocol(Protocol.HTTP_1_1)
                .code(httpCode)
                .message("message")
                .build();
    }

}
