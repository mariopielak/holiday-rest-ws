package pl.mariopielak.holiday.test;

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.util.Objects;
import pl.mariopielak.holiday.json.HolidayResponse;

import java.time.LocalDate;

public class HolidayResponseAssert extends AbstractAssert<HolidayResponseAssert, HolidayResponse> {

    public HolidayResponseAssert hasName1(String name1) {
        isNotNull();
        if (!Objects.areEqual(actual.getName1(), name1)) {
            failWithMessage("Expected name1 to be <%s> bu was <%s>", actual.getName1(), name1);
        }
        return this;
    }

    public HolidayResponseAssert hasName2(String name2) {
        isNotNull();
        if (!Objects.areEqual(actual.getName2(), name2)) {
            failWithMessage("Expected name2 to be <%s> bu was <%s>", actual.getName2(), name2);
        }
        return this;
    }

    public HolidayResponseAssert hasDate(LocalDate date) {
        isNotNull();
        if (!Objects.areEqual(actual.getDate(), date)) {
            failWithMessage("Expected date to be <%tD> but was <%tD>", actual.getDate(), date);
        }
        return this;
    }

    public HolidayResponseAssert(HolidayResponse response) {
        super(response, HolidayResponseAssert.class);
    }

    public static HolidayResponseAssert assertThat(HolidayResponse response) {
        return new HolidayResponseAssert(response);
    }
}
