package pl.mariopielak.holiday.resource;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.mariopielak.holiday.json.HolidayResponse;
import pl.mariopielak.holiday.service.HolidayService;

import java.time.LocalDate;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class HolidayResourceTest {

    @Mock
    private HolidayService holidayServiceMock;

    @Mock
    private HolidayResponse commonHolidayResponseMock;

    @InjectMocks
    private HolidayResource objectUnderTest;

    private MockMvc mockMvc;

    @Before
    public void initialize() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(objectUnderTest).build();
    }

    @Test
    public void shouldFindNextHolidayAndReturnData() throws Exception {
        // given
        when(holidayServiceMock.findCommonHoliday("US", "PL", LocalDate.of(2016, 1, 1))).thenReturn(
                new HolidayResponse(LocalDate.of(2016, 1, 1), "New Year", "Nowy Rok"));
        // when
        mockMvc.perform(get("/holiday/findCommonHoliday?firstCountryCode={x}&secondCountryCode={y}&date={z}",
                "US", "PL", "2016-01-01"))
                // then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name1", is("New Year")))
                .andExpect(jsonPath("$.name2", is("Nowy Rok")))
                .andExpect(jsonPath("$.date", is("2016-01-01")));
    }

    @Test
    public void shouldThrowAnExceptionWhenRequiredParametersAreMissing() throws Exception {
        // given
        // when
        mockMvc.perform(get("/holiday/findCommonHoliday"))
                // then
                .andExpect(status().is4xxClientError());
    }

}
