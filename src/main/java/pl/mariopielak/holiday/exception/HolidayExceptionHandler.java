package pl.mariopielak.holiday.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class HolidayExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({HolidayException.class})
    public ResponseEntity<Object> handleAccessDeniedException(Exception ex, WebRequest request) {
        return new ResponseEntity<>("We are very sorry :( Holiday service information can't find common holiday!",
                new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
