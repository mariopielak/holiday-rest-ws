package pl.mariopielak.holiday.exception;

public class HolidayException extends RuntimeException {

    public HolidayException(String message) {
        super(message);
    }

    public HolidayException(Exception e) {
        super(e);
    }
}
