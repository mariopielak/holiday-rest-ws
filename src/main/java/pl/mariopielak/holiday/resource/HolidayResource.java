package pl.mariopielak.holiday.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import pl.mariopielak.holiday.json.HolidayResponse;
import pl.mariopielak.holiday.service.HolidayService;

import java.time.LocalDate;

@RestController
@RequestMapping("/holiday")
public class HolidayResource {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private HolidayService holidayService;

    @Autowired
    public HolidayResource(HolidayService holidayService) {
        this.holidayService = holidayService;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/findCommonHoliday")
    @ResponseBody
    public HolidayResponse findCommonHoliday(
            @RequestParam(name = "firstCountryCode") final String firstCountryCode,
            @RequestParam(name = "secondCountryCode") final String secondCountryCode,
            @RequestParam(name = "date") @DateTimeFormat(pattern = "yyyy-MM-dd") final LocalDate date) {
        logger.debug("Received call via REST /findCommonHoliday, firstCountryCode={}, secondCountryCode={}, date={}",
                firstCountryCode, secondCountryCode, date);
        return holidayService.findCommonHoliday(firstCountryCode, secondCountryCode, date);
    }

}
