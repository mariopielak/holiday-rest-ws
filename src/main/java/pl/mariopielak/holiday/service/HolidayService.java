package pl.mariopielak.holiday.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mariopielak.holiday.exception.HolidayException;
import pl.mariopielak.holiday.json.HolidayResponse;
import pl.mariopielak.holiday.json.CountryHoliday;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class HolidayService {

    private CountryHolidayWSClient wsClient;

    @Autowired
    public HolidayService(CountryHolidayWSClient wsClient) {
        this.wsClient = wsClient;
    }

    public HolidayResponse findCommonHoliday(final String firstCountryCode, final String secondCountryCode, final LocalDate date) {
        Optional<HolidayResponse> holidayResponse = getCommonHoliday(firstCountryCode, secondCountryCode, date);
        if (!holidayResponse.isPresent()) {
            // check for current and the next year
            holidayResponse = getCommonHoliday(firstCountryCode, secondCountryCode, LocalDate.of(date.getYear() + 1, 1, 1));
        }
        return holidayResponse.orElseThrow(() -> new HolidayException(
                String.format("Can't find common holiday for given countries : %s, %s in date : %s", firstCountryCode, secondCountryCode, date)));

    }

    private Optional<HolidayResponse> getCommonHoliday(final String firstCountryCode, final String secondCountryCode, final LocalDate date) {
        List<CountryHoliday> firstHolidays = getHolidaysByYear(firstCountryCode, date);
        List<CountryHoliday> secondHoidays = getHolidaysByYear(secondCountryCode, date);
        Optional<LocalDate> commonHoliday = getCommonHoliday(firstHolidays, secondHoidays, date);
        if (!commonHoliday.isPresent()) {
            return Optional.empty();
        }
        return Optional.of(new HolidayResponse(commonHoliday.get(),
                getHolidayName(firstHolidays, commonHoliday.get()),
                getHolidayName(secondHoidays, commonHoliday.get())));
    }


    private String getHolidayName(List<CountryHoliday> holidays, LocalDate date) {
        return holidays.stream()
                .filter(r -> r.getDate().isEqual(date))
                .findFirst().get().getName();
    }

    private Optional<LocalDate> getCommonHoliday(final List<CountryHoliday> firstHolidays,
                                                 final List<CountryHoliday> secondHolidays, final LocalDate date) {

        List<LocalDate> firstDateOfHolidays = firstHolidays.stream()
                .map(CountryHoliday::getDate)
                .collect(Collectors.toList());

        return secondHolidays.stream()
                .map(CountryHoliday::getDate)
                .filter(e -> firstDateOfHolidays.contains(e) && !e.isBefore(date))
                .sorted()
                .findFirst();
    }

    private List<CountryHoliday> getHolidaysByYear(final String countryCode, final LocalDate date) {
        return wsClient.getHolidays(countryCode, date.getYear());
    }

}
