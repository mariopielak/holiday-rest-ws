package pl.mariopielak.holiday.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import pl.mariopielak.holiday.exception.HolidayException;
import pl.mariopielak.holiday.json.CountryHoliday;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class CountryHolidayParser {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final ObjectMapper objectMapper;

    @Autowired
    public CountryHolidayParser(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    List<CountryHoliday> tryToParseResponse(final Response response) {
        try {
            return parseResponse(response);
        } catch (IOException e) {
            logger.error("Error occurred during parsing response from external holiday information service", e);
            throw new HolidayException(e);
        }
    }

    private List<CountryHoliday> parseResponse(final Response response) throws IOException {
        if (response == null || response.body() == null) {
            throw new HolidayException("Received response is empty");
        }
        String responseAsText = response.body().string();
        JsonNode root = objectMapper.readTree(responseAsText);
        if (!isSuccessResponse(root)) {
            throw new HolidayException("Received response is wrong, response :" + responseAsText);
        }
        Iterator<JsonNode> jsonNodeIterator = root.findValue("holidays").elements();
        List<CountryHoliday> responses = new ArrayList<>();
        while(jsonNodeIterator.hasNext()) {
            JsonNode jsonNode = jsonNodeIterator.next();
            responses.add(new CountryHoliday(jsonNode.findValue("name").asText(),
                    LocalDate.parse(jsonNode.findValue("date").asText())));
        }
        return responses;
    }


    private boolean isSuccessResponse(final JsonNode root) {
        return HttpStatus.valueOf(Integer.parseInt(root.findValue("status").asText())).is2xxSuccessful();
    }
}
