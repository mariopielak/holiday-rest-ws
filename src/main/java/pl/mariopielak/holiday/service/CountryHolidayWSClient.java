package pl.mariopielak.holiday.service;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.mariopielak.holiday.exception.HolidayException;
import pl.mariopielak.holiday.json.CountryHoliday;

import java.io.IOException;
import java.util.List;

@Service
public class CountryHolidayWSClient {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${holiday.api.address}")
    private String holidayApiAddress;

    @Value("${holiday.api.key}")
    private String holidayApiKey;

    private OkHttpClient okHttpClient;

    private CountryHolidayParser responseParser;

    @Autowired
    public CountryHolidayWSClient(OkHttpClient okHttpClient, CountryHolidayParser responseParser) {
        this.okHttpClient = okHttpClient;
        this.responseParser = responseParser;
    }

    public List<CountryHoliday> getHolidays(final String countryCode, final int year) {
        logger.debug("Trying to find holiday for countryCode : {}, and year : {}", countryCode, year);
        Request request = buildRequest(countryCode, year);
        Response response = callExternalWS(request);
        logger.debug("Receive response : " + response);
        return responseParser.tryToParseResponse(response);
    }

    private Response callExternalWS(final Request request) {
        try {
            return okHttpClient.newCall(request).execute();
        } catch (IOException e) {
            logger.error("Error occurred during calling http get method of external holiday information service", e);
            throw new HolidayException(e);
        }
    }

    private Request buildRequest(final String countryCode, final int year) {
        return new Request.Builder()
                .url(buildRequestUrl(countryCode, year))
                .build();
    }

    private HttpUrl buildRequestUrl(final String countryCode, final int year) {
        return HttpUrl.parse(holidayApiAddress).newBuilder()
                .addQueryParameter("key", holidayApiKey)
                .addQueryParameter("country", countryCode)
                .addQueryParameter("year", String.valueOf(year)).build();
    }
}
