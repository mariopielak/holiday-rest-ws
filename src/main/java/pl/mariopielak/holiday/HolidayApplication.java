package pl.mariopielak.holiday;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import pl.mariopielak.holiday.configuration.HolidayConfiguration;

@SpringBootApplication
@EnableAutoConfiguration
@Import(HolidayConfiguration.class)
public class HolidayApplication {

    public static void main(String[] args) {
        SpringApplication.run(HolidayApplication.class, args);
    }
}
