package pl.mariopielak.holiday.json;

import java.time.LocalDate;
import java.util.Objects;

public class CountryHoliday {

    private final String name;

    private final LocalDate date;

    public CountryHoliday(String name, LocalDate date) {
        this.name = name;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryHoliday that = (CountryHoliday) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, date);
    }
}
