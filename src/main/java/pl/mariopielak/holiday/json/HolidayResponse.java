package pl.mariopielak.holiday.json;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.time.LocalDate;

public class HolidayResponse {

    @JsonSerialize(using = ToStringSerializer.class)
    private final LocalDate date;

    private final String name1;

    private final String name2;

    public HolidayResponse(LocalDate date, String name1, String name2) {
        this.date = date;
        this.name1 = name1;
        this.name2 = name2;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getName1() {
        return name1;
    }

    public String getName2() {
        return name2;
    }
}
